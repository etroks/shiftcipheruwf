﻿namespace ShiftCipherUWF
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTextBox = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SelectFile = new System.Windows.Forms.Button();
            this.DecryptionAndView = new System.Windows.Forms.Button();
            this.InfoTextBox1 = new System.Windows.Forms.TextBox();
            this.View5Var = new System.Windows.Forms.Button();
            this.ViewAllVar = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.SaveVar = new System.Windows.Forms.Button();
            this.ChangedVar = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.Encryption = new System.Windows.Forms.Button();
            this.Bias = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // MainTextBox
            // 
            this.MainTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllSystemSources;
            this.MainTextBox.Location = new System.Drawing.Point(-5, -4);
            this.MainTextBox.Multiline = true;
            this.MainTextBox.Name = "MainTextBox";
            this.MainTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.MainTextBox.Size = new System.Drawing.Size(1584, 773);
            this.MainTextBox.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SelectFile
            // 
            this.SelectFile.Location = new System.Drawing.Point(1593, 12);
            this.SelectFile.Name = "SelectFile";
            this.SelectFile.Size = new System.Drawing.Size(293, 85);
            this.SelectFile.TabIndex = 1;
            this.SelectFile.Text = "Выбрать файл для обработки";
            this.SelectFile.UseVisualStyleBackColor = true;
            this.SelectFile.Click += new System.EventHandler(this.SelectFile_Click);
            // 
            // DecryptionAndView
            // 
            this.DecryptionAndView.Location = new System.Drawing.Point(1593, 103);
            this.DecryptionAndView.Name = "DecryptionAndView";
            this.DecryptionAndView.Size = new System.Drawing.Size(293, 85);
            this.DecryptionAndView.TabIndex = 2;
            this.DecryptionAndView.Text = "Дешифровать и вывести информаци на экран";
            this.DecryptionAndView.UseVisualStyleBackColor = true;
            this.DecryptionAndView.Click += new System.EventHandler(this.DecryptionAndView_Click);
            // 
            // InfoTextBox1
            // 
            this.InfoTextBox1.BackColor = System.Drawing.SystemColors.Window;
            this.InfoTextBox1.Location = new System.Drawing.Point(1593, 194);
            this.InfoTextBox1.Multiline = true;
            this.InfoTextBox1.Name = "InfoTextBox1";
            this.InfoTextBox1.ReadOnly = true;
            this.InfoTextBox1.Size = new System.Drawing.Size(293, 68);
            this.InfoTextBox1.TabIndex = 3;
            this.InfoTextBox1.Text = "Если информация дешифрована некорректно, выберете одно из следующих действий:";
            this.InfoTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // View5Var
            // 
            this.View5Var.Location = new System.Drawing.Point(1593, 268);
            this.View5Var.Name = "View5Var";
            this.View5Var.Size = new System.Drawing.Size(140, 85);
            this.View5Var.TabIndex = 4;
            this.View5Var.Text = "Показать 5 вариантов";
            this.View5Var.UseVisualStyleBackColor = true;
            this.View5Var.Click += new System.EventHandler(this.View5Var_Click);
            // 
            // ViewAllVar
            // 
            this.ViewAllVar.Location = new System.Drawing.Point(1746, 268);
            this.ViewAllVar.Name = "ViewAllVar";
            this.ViewAllVar.Size = new System.Drawing.Size(140, 85);
            this.ViewAllVar.TabIndex = 5;
            this.ViewAllVar.Text = "Показать все варианты";
            this.ViewAllVar.UseVisualStyleBackColor = true;
            this.ViewAllVar.Click += new System.EventHandler(this.ViewAllVar_Click);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(1593, 359);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(293, 85);
            this.Save.TabIndex = 6;
            this.Save.Text = "Сохранить предложенный вариант";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // SaveVar
            // 
            this.SaveVar.Location = new System.Drawing.Point(1593, 481);
            this.SaveVar.Name = "SaveVar";
            this.SaveVar.Size = new System.Drawing.Size(293, 85);
            this.SaveVar.TabIndex = 7;
            this.SaveVar.Text = "Сохранить вариант, указанный в окне ниже";
            this.SaveVar.UseVisualStyleBackColor = true;
            this.SaveVar.Click += new System.EventHandler(this.SaveVar_Click);
            // 
            // ChangedVar
            // 
            this.ChangedVar.Location = new System.Drawing.Point(1593, 572);
            this.ChangedVar.Name = "ChangedVar";
            this.ChangedVar.Size = new System.Drawing.Size(293, 26);
            this.ChangedVar.TabIndex = 8;
            this.ChangedVar.Text = "Укажите вариант, для сохранения";
            this.ChangedVar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ChangedVar.TextChanged += new System.EventHandler(this.ChangedVar_TextChanged);
            // 
            // Encryption
            // 
            this.Encryption.Location = new System.Drawing.Point(1593, 642);
            this.Encryption.Name = "Encryption";
            this.Encryption.Size = new System.Drawing.Size(293, 81);
            this.Encryption.TabIndex = 9;
            this.Encryption.Text = "Зашифровать введенный текст";
            this.Encryption.UseVisualStyleBackColor = true;
            this.Encryption.Click += new System.EventHandler(this.Encryption_Click);
            // 
            // Bias
            // 
            this.Bias.Location = new System.Drawing.Point(1593, 729);
            this.Bias.Name = "Bias";
            this.Bias.Size = new System.Drawing.Size(293, 26);
            this.Bias.TabIndex = 10;
            this.Bias.Text = "Укажите смещение";
            this.Bias.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Bias.TextChanged += new System.EventHandler(this.Bias_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1898, 769);
            this.Controls.Add(this.Bias);
            this.Controls.Add(this.Encryption);
            this.Controls.Add(this.ChangedVar);
            this.Controls.Add(this.SaveVar);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.ViewAllVar);
            this.Controls.Add(this.View5Var);
            this.Controls.Add(this.InfoTextBox1);
            this.Controls.Add(this.DecryptionAndView);
            this.Controls.Add(this.SelectFile);
            this.Controls.Add(this.MainTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MainTextBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button SelectFile;
        private System.Windows.Forms.Button DecryptionAndView;
        private System.Windows.Forms.TextBox InfoTextBox1;
        private System.Windows.Forms.Button View5Var;
        private System.Windows.Forms.Button ViewAllVar;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button SaveVar;
        private System.Windows.Forms.TextBox ChangedVar;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button Encryption;
        private System.Windows.Forms.TextBox Bias;
    }
}

