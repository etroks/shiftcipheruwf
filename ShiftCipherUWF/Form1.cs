﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShiftCipherUWF
{
    public partial class Form1 : Form
    {
        private string pathLoad;
        private int changedVar;
        private int bias = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void SelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string path = openFileDialog1.FileName;
            if ((Path.GetExtension(path) != ".txt") & (Path.GetExtension(path) != ".docx"))
                MainTextBox.Text = "Выбран не верный файл";
            else
            {
                pathLoad = path;
                if (Path.GetExtension(pathLoad) == ".txt")
                    Program.workingText.ReadTXT(pathLoad);
                if (Path.GetExtension(pathLoad) == ".docx")
                    Program.workingText.ReadDocx(pathLoad);
            }
        }

        private void DecryptionAndView_Click(object sender, EventArgs e)
        {
            if (pathLoad == null) MainTextBox.Text = "Не выбран файл для дешифровки";
            else
                MainTextBox.Text = Program.workingText.Decryption(1);
        }

        private void View5Var_Click(object sender, EventArgs e)
        {
            if (Program.workingText.Text == null) MainTextBox.Text = "Не выбран файл для дешифровки";
            else
                MainTextBox.Text = Program.workingText.Decryption(5);
        }

        private void ViewAllVar_Click(object sender, EventArgs e)
        {
            if (Program.workingText.Text == null) MainTextBox.Text = "Не выбран файл для дешифровки";
            else
                MainTextBox.Text = Program.workingText.Decryption(33);
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (Program.workingText.Text == null) MainTextBox.Text = "Нечего сохранять";
            else
            {
                saveFileDialog1.ShowDialog();
                Program.workingText.SaveTXT(saveFileDialog1.FileName, Program.workingText.Text);
            }
        }

        private void SaveVar_Click(object sender, EventArgs e)
        {
            if (Program.workingText.Text == null) MainTextBox.Text = "Нечего сохранять";
            else
            {
                saveFileDialog1.ShowDialog();
                Program.workingText.SaveTXT(saveFileDialog1.FileName, Program.workingText.Encryption(Program.workingText.BiasMain + changedVar - 1, Program.workingText.Text));
            }
        }

        private void ChangedVar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                changedVar = Convert.ToInt32(ChangedVar.Text);
            }
            catch { MainTextBox.Text = "Ошибка ввода нужного варианта"; }
        }

        private void Encryption_Click(object sender, EventArgs e)
        {
            if (MainTextBox.Text == null) MainTextBox.Text = "Нечего шифровать";
            else
            {
                saveFileDialog1.ShowDialog();
                Program.workingText.SaveTXT(saveFileDialog1.FileName, Program.workingText.Encryption(bias, MainTextBox.Text));
            }
        }

        private void Bias_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bias = Convert.ToInt32(Bias.Text);
            }
            catch { MainTextBox.Text = "Ошибка ввода смещения"; }
        }
    }
}