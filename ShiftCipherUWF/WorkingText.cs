﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ShiftCipherUWF
{
    class WorkingText
    {
        private char[] letters = new char[67]{'0', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О',
            'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я',
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о',
            'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'};
        private int biasMain;
        private string text;

        public string Text { get => text;}
        public int BiasMain { get => biasMain;}

        public string Encryption(int bias, string text)
        {
            while (bias > 33) bias = bias - 33;

            StringBuilder sb = new StringBuilder();

            foreach (char letter in text)
            {
                if (letters.Contains(letter))//если это буква
                {
                    int index = Array.IndexOf(letters, letter);
                    if (index < 34)//если это большая буква
                    {
                        if ((index + bias) >= 33)//если выходит за рамки больших букв
                            sb.Append(letters[index + bias - 33]);
                        else sb.Append(letters[index + bias]);//если не выходит за рамки больших букв
                    }
                    else//если это малая буква
                    {
                        if ((index + bias) >= 67)//если выходит за рамки малых букв
                            sb.Append(letters[index + bias - 33]);
                        else sb.Append(letters[index + bias]);//если не выходит за рамки малых букв
                    }
                }
                else sb.Append(letter);
            }
            return sb.ToString();
        }

        public string Decryption(int variation)
        {
            StringBuilder cipherSB = new StringBuilder();
            string cipher = Text.ToLower();
            int[] letter = new int[34];
            int indexMax;
            int bias = 0;

            for (int i = 35; i < 67; i++)
                letter[i - 34] = cipher.Split(letters[i]).Length - 1;

            indexMax = Array.IndexOf(letter, letter.Max()) + 1;

            if (16 > indexMax) bias = 16 - indexMax;
            if (16 < indexMax) bias = 33 - indexMax + 16;
            if (16 == indexMax) bias = 0;
            biasMain = bias;

            for (int i = 0; i < variation; i++)
            {
                bias += i;

                cipherSB.Append(Encryption(bias, Program.workingText.Text) + "\r\n\r\n");

                letter[indexMax] = 0;
            }

            return cipherSB.ToString();
        }

        public void ReadTXT(string path)
        {
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() > 0)
                {
                    sb.Append(sr.ReadLine() + "\r\n");
                }
            }
            text = sb.ToString();
        }

        public void SaveTXT(string path, string text)
        {
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.Write(text.ToString());
            }
        }

        public void ReadDocx(string path)
        {
            XmlNamespaceManager NsMgr = new XmlNamespaceManager(new NameTable());
            NsMgr.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

            using (var archive = ZipFile.OpenRead(path))
            {
                text = XDocument
                    .Load(archive.GetEntry(@"word/document.xml").Open())
                    .XPathSelectElements("//w:p", NsMgr)
                    .Aggregate(new StringBuilder(), (sb, p) => p
                        .XPathSelectElements(".//w:t|.//w:tab|.//w:br", NsMgr)
                        .Select(e => { switch (e.Name.LocalName) { case "br": return "\v"; case "tab": return "\t"; } return e.Value; })
                        .Aggregate(sb, (sb1, v) => sb1.Append(v)))
                    .ToString();
            }
        }
    }
}
